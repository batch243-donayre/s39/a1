const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/userControllers")
	// Route for checking Email
	router.post("/checkEmail", userController.checkEmailExists);
	// route for registration
	router.post("/register", userController.checkEmailExists,userController.registerUser);
	// route for log in
	router.post("/login", userController.loginUser);

	// route for retrieving user details
	router.get("/details",auth.verify,userController.userDetails);

	router.get("/profile", userController.profileDetails);

module.exports = router;