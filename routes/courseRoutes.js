const express = require ("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js");

router.post("/", auth.verify,courseControllers.addCourse);

router.get("/allActiveCourses", courseControllers.getAllActive);

//Specific course
router.get("/:courseId", courseControllers.getCourse);

//update specific course
router.put("/update/:courseId", auth.verify, courseControllers.updateCourse);

//Archiving course
router.patch("/:courseId/archive", auth.verify, courseControllers.archivingCourse);



module.exports = router;